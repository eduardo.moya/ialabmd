import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from './components/home/Home'
import Layout from './components/Layout'
import MdPost from './components/mdPost/MdPost'
import { FilesProvider, IndexProvider } from './context/context'
import React from 'react'
import Events from './components/posts/Events'

function App() {
	return (
		<IndexProvider>
		<FilesProvider>
		<BrowserRouter basename={process.env.PUBLIC_URL}>
		<Routes>
			<Route path='/' element={<Layout />}>
				<Route index element={<Home />} />
				<Route path='/publicaciones'>
					<Route path=':fileName' element={<MdPost currentDir={'publicaciones'}/>}></Route>
				</Route>
				<Route path='/eventos' element={<Events />}/>
			</Route>
		</Routes>
		</BrowserRouter>
		</FilesProvider>
		</IndexProvider>
	);
}

export default App;

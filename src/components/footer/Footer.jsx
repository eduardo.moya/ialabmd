import { Dribble, Facebook, Github, Instagram, Twitter } from "../../lib/svg"

function Footer() {
	return (
		<footer className='w-full h-auto p-7 bg-[#232a34] bottom-0'>
			<div className="max-w-screen-xl px-4 py-9 mx-auto space-y-8 overflow-hidden sm:px-6 lg:px-8">
				<nav className="flex flex-wrap justify-center -mx-5 -my-2">
					<div className="px-5 py-2">
						<p href="#" className="text-base leading-6 text-gray-500 hover:text-gray-900">
							About
						</p>
					</div>
					<div className="px-5 py-2">
						<p href="#" className="text-base leading-6 text-gray-500 hover:text-gray-900">
							Blog
						</p>
					</div>
					<div className="px-5 py-2">
						<p href="#" className="text-base leading-6 text-gray-500 hover:text-gray-900">
							Team
						</p>
					</div>
					<div className="px-5 py-2">
						<p href="#" className="text-base leading-6 text-gray-500 hover:text-gray-900">
							Contact
						</p>
					</div>
					<div className="px-5 py-2">
						<p href="#" className="text-base leading-6 text-gray-500 hover:text-gray-900">
							Terms
						</p>
					</div>
				</nav>
				<div className="flex justify-center mt-8 space-x-6">
					<p href="#" className="text-gray-400 hover:text-gray-500">
						<span className="sr-only">Facebook</span>
						<Facebook />
					</p>
					<p href="#" className="text-gray-400 hover:text-gray-500">
						<span className="sr-only">Instagram</span>
						<Instagram />
					</p>
					<p href="#" className="text-gray-400 hover:text-gray-500">
						<span className="sr-only">Twitter</span>
						<Twitter />
					</p>
					<p href="#" className="text-gray-400 hover:text-gray-500">
						<span className="sr-only">GitHub</span>
						<Github />
					</p>
					<p href="#" className="text-gray-400 hover:text-gray-500">
						<span className="sr-only">Dribbble</span>
						<Dribble />
					</p>
				</div>
				<p className="mt-8 text-base leading-6 text-center text-gray-400">
					© 2023 SomeCompany, Inc. All rights reserved.
				</p>
			</div>
		</footer>
	)
}

export default Footer
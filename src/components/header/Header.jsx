import { useState } from "react"
import { Burger } from "../../lib/svg"
import Logo from "../navbar/Logo"
import { SearchBox } from "../navbar/SearchBox"

export function Header() {
	const [isOpen, setIsOpen] = useState(false)
	const routes = [
		{ name: 'Página principal', link: '/' },
		{ name: 'Enlaces externos', link: '/' },
		{ name: 'Acerca de', link: '/' },
	]

	return (
		<header className='relative flex p-4'>
			<nav className='flex gap-4 w-full mx-auto my-0 items-center justify-between'>
				<div className='flex w-1/5'>
					<Logo />
				</div>

				<ul className='hidden md:flex md:w-2/5 gap-8 justify-center text-xl font-bold dark:text-slate-100'>
					{routes.map(item => (
						<p key={item.name}>{item.name}</p>
					))}
				</ul>

				<div className='px-4 py-2 hidden md:flex md:w-1/5 md:justify-end'>
					<SearchBox />
				</div>

				<button className='block md:hidden cursor-pointer'
					onClick={() => setIsOpen(!isOpen)}
				>
					<Burger className={'w-8 h-8 fill-current text-black dark:text-white'} viewBox={'0 0 20 20'} />
				</button>
			</nav>


			<div className={`dropdown_menu absolute z-40 p-2 right-0 top-16 w-full h-[300px] text-white bg-[#222e39] dark:bg-[#222e39] ${isOpen ? 'flex flex-col gap-4 h-60' : 'hidden'}`}>
				<div className='mx-auto w-10/12 mt-4'>
					<SearchBox />
				</div>

				{routes.map(item => (
					<li key={item.name} className='flex justify-center text-lg font-bold dark:text-slate-100'>{item.name}</li>
				))}
			</div>
		</header>
	)
}

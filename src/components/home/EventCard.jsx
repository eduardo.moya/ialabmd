import { useEffect, useState } from 'react'
import { parseEvent, trimFileExtension } from '../../lib/text'
import { useGetMarkdown } from '../../hooks/hooks'
import dayjs from 'dayjs'
import 'dayjs/locale/es-mx'
import { fechaEN } from '../../lib/time'
import { LoadingSpinner } from '../../lib/svg'


export default function EventCard({ item }) {
	const [markdown, setMarkdown] = useState()
	const [loading, setLoading] = useState()
	const [event, setEvent] = useState()

	dayjs.locale('es-mx')
	
	useGetMarkdown({ setLoading, setMarkdown, fileName: item.name, path: 'eventos'})
	
	useEffect(() => {
		setEvent(current => parseEvent(markdown))
	}, [markdown])
		
	const getDescription = item => {
		const arr = item?.lines || ['']
		return arr.slice(
			arr.indexOf('## Descripcion'),
			arr.indexOf('## Descripcion') + 2
		)[1] || 'Sin descripción'
	}

	const getDate = (item, format = 'D MMM YYYY') => {
		if(!item) return
		
		const fecha = item.fecha || item.Fecha
		return dayjs(fechaEN(fecha), { locale: 'es-mx' }).format(format)
	}

	return (
		<>
			{loading ? 
				<LoadingSpinner />
				:
				<div className='flex flex-wrap py-2 px-1 w-full'>
					<div className='w-2/3 p-1'>
						<h4 className='font-bold text-lg'>
							{trimFileExtension(item?.name) || ' '}
						</h4>
						<div className='max-h-12 overflow-hidden'>
							{getDescription(item) || ' '}
						</div>
					</div>

					<div className='flex flex-col items-center h-full w-1/3 p-1 text-center order-first'>
						<div className='flex flex-col w-14 items-center rounded-lg bg-red-800'>
							<div className='font-bold text-white'>
								{getDate(event, 'MMM') || '-'}
							</div>
							<div className='w-10/12 mb-1 pt-1 rounded bg-white text-black text-lg font-bold'>
								{getDate(event, 'D') || '-'}
							</div>
						</div>
						<div className='text-sm p-1'>
							{getDate(event, 'h:MM a') || '-'}
						</div>
					</div>
				</div>
			}
		</>
	)
}

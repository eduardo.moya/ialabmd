import { useState } from 'react'
import { useListPublications } from '../../hooks/hooks'
import EventCard from './EventCard'
import { LoadingSpinner } from '../../lib/svg'

export default function Events({ showSidebar, setShowsidebar }) {
	const [fileList, setFileList] = useState([])
	const [loading, setLoading] = useState(true)

	useListPublications({ setFileList, setLoading, path:'eventos' })

	return (
		<>
			<div className={`fixed xl:static ${showSidebar? 'flex bg-slate-100 dark:bg-[#0d1117]' : 'hidden'} xl:flex top-0 xl:top-auto z-30 w-full xl:w-72 h-screen xl:h-auto justify-center overflow-auto`}>
				{loading ? 
					<LoadingSpinner 
						className=' w-10 h-10 animate-spin fill-blue-400 dark:fill-slate-100' 
						viewBox='0 0 20 20' 
					/>
					:
					<nav className='min-w-[250px] max-w-[800px] px-2'>
						<h3 className='w-full px-2 py-1 border-b-2 border-slate-500'>
							Próximos Eventos
						</h3>

						<div id='eventos_proximos' className='divide-y divide-dashed'>
							{fileList.length > 0 ?
								fileList.map( (item, i) => (<EventCard key={i} item={item} />))
								:
								<p>No hay eventos</p>
							}
						</div>

						{/* <h3 className='w-full p-2 border-b-2 border-slate-500'>
							Eventos pasados
						</h3>

						<div id='eventos_pasados' className='divide-y divide-dashed pb-16'>
							<EventCard />
							
						</div> */}
					</nav>
				}
			</div>

			<button className='fixed flex z-40 xl:hidden bottom-3 left-3 lg:bottom-6 lg:left-6 p-2 bg-gray-600'
				onClick={() => setShowsidebar( current => !current)}
			>
				Events
			</button>
		</>
	)
}

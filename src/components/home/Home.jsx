import { useState } from 'react'
import Publications from '../posts/Publications'
import Events from './Events'
import { useEffect } from 'react'
import { useRef } from 'react'
import { useResize } from '../../hooks/hooks'
import { useIndex } from '../../context/context'

export default function Home() {
	const [showSidebar, setShowsidebar] = useState(false)
	const mainContent = useRef() 
	const { overflowRef } = useIndex()
	const windowSize = useResize(overflowRef, [
		{ sm: 640 },
		{ md: 768 },
		{ lg: 1024 },
		{ xl: 1279 },
		{ xxl: 1536 },
	])

	useEffect(() => {
		if(windowSize === 'xxl') setShowsidebar(false)
	}, [windowSize])

	return (
		<div ref={mainContent} className='flex flex-wrap'>
			<Events showSidebar={showSidebar} setShowsidebar={setShowsidebar} />
			<main className={`flex flex-1 items-start ${showSidebar? 'overflow-hidden fixed' : ''}`}>
				<Publications />
			</main>
			<div className='hidden xl:block xl:w-64'></div>
		</div>
	)
}

import React from 'react'
import Markdown from 'markdown-to-jsx'

export const RenderMd = ({ source }) => {
	return (
		<Markdown className='markdown'>{source}</Markdown>
	)
}

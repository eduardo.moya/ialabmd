import MarkdownPreview from '@uiw/react-markdown-preview'
import { useEffect } from 'react'
import { getNestedHeadings } from '../../../lib/utils'
import { trimFileExtension } from '../../../lib/text'

const BASE_URL = 'https://gitlab.com/Atreston/ialabmd/-/raw'
const BRANCH = 'pages'

export const RenderMd = ({ source, title, setHeadings, currentDir }) => {
	const isURL = (string) => string.includes('http')

	useEffect(() => {
		const headingElements = Array.from(
			document.querySelectorAll("h2, h3, h4")
		)
		setHeadings(getNestedHeadings(headingElements))
	}, [source, setHeadings])

	return (
		<div>
			<h1 className='text-center text-4xl pt-8 pb-5 font-semibold'>{trimFileExtension(title)}</h1>

			<MarkdownPreview
				className='markdownView'
				source={source}
				transformImageUri={(src) => {
					if (isURL(src)) return src
					else return `${BASE_URL}/${BRANCH}/${currentDir}/${src}`
				}}
			/>
		</div>
	)
}

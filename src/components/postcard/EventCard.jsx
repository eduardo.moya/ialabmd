import { Link } from 'react-router-dom'
import { CalendarA, ClockA, LocationA, PavelKryutsouCC } from '../../lib/svg'
import Tooltip from '../../lib/tooltip/Tooltip'

export default function EventCard() {
	return (
		<div className='flex xl:w-11/12 rounded-xl shadow-md bg-white dark:bg-slate-900'>
			{/* Left side */}
			<div className='flex flex-col justify-center items-center gap-2 mx-3 p-4 xl:w-1/6'>
				<h2 className='px-2 py-2 rounded-lg text-white bg-slate-600'>
					<span className='text-4xl font-bold'>23</span>
				</h2>
				<h3>OCT</h3>
			</div>

			{/* Right side */}
			<div className='flex flex-col gap-2 m-3 xl:w-5/6'>
				{/* Title */}
				<h4 className='text-2xl uppercase'><strong>Ice Cream Social</strong></h4>
				
				{/* Details */}
				<ul className='flex flex-col sm:flex-row sm:gap-5'>
					<li className='flex gap-2 items-center'>
						<CalendarA className='fill-white w-6 h-6 mb-1' stroke='gray'/> Monday
					</li>
					<li className='flex gap-2 items-center'>
						<ClockA className='w-5 h-5 mb-1'/> 12:30 PM - 2:00 PM
					</li>
					<li className='flex gap-2 items-center'>
						<Tooltip content={<PavelKryutsouCC />} direction='left'>
							<Link to={'https://dribbble.com/paulpatap?ref=svgrepo.com'}>
								<LocationA className='w-7 h-7 mb-1' fill='white' stroke='black'/>
							</Link>
						</Tooltip> Cafe
					</li>
				</ul>
				
				{/* Description */}
				<p className='max-h-12 overflow-hidden'>Lorem ipsum dolsit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			</div>
		</div>
	)
}

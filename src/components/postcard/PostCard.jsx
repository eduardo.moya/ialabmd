import { getTimeLapse } from '../../lib/time'

export default function PostCard({ markdown }) {
	const trimFileExtension = input => input.substr(0, input.lastIndexOf('.')) || input

	return (
		<div className='mx-auto my-3 rounded-xl p-5 shadow-md w-10/12 bg-white dark:bg-[#282541]'>
			<div className='mt-4 mb-6'>
				{markdown.name ?
					<h2 className='mb-3 text-xl font-bold dark:text-[#c9d1d9]'>{trimFileExtension(markdown.name)}</h2>
					:
					<h2 className='mb-3 text-xl font-bold'>...</h2>
				}
			</div>

			<div className='flex w-full items-center justify-end gap-2 border-t pb-1'>
				<div className='flex items-center space-x-3'>
					{markdown.author ?
						<h3 className='text-lg font-bold text-slate-700 dark:text-[#3969ac]'>{markdown.author}</h3>
						:
						<h3 className='text-lg font-bold text-slate-700 dark:text-[#3969ac]'>...</h3>
					}
				</div>

				<div className='flex items-center space-x-8'>
					{markdown.date ?
						<p className='text-xs text-neutral-500 dark:text-neutral-300'>
							{getTimeLapse(markdown.date)}
						</p>
						:
						<p className='text-xs text-neutral-500 dark:text-neutral-300'>No date was given</p>
					}
				</div>
			</div>
		</div>
	)
}

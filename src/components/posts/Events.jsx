import { useState } from 'react'
import { useListPublications } from '../../hooks/hooks'
import EventCard from '../postcard/EventCard'
import { LoadingSpinner } from '../../lib/svg'

export default function Events() {
	const [eventsList, setFileList] = useState([])
	const [loading, setLoading] = useState(true)
	useListPublications({ setFileList, setLoading, path: 'eventos' })

	return (
		<>
			{loading && eventsList?
				<LoadingSpinner />
				:
				<div className='flex flex-col gap-4 px-3 my-3 items-center self-center md:w-5/6'>
					<EventCard />
					<EventCard />
					<EventCard />
					<EventCard />
					<EventCard />
					<EventCard />
					<EventCard />
					<EventCard />
					<EventCard />
					<EventCard />
					<EventCard />
				</div>
			}
		</>
	)
}

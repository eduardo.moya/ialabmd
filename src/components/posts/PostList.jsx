import { Link } from 'react-router-dom'
import InfiniteScroll from 'react-infinite-scroll-component'
import { LoadingSpinner } from '../../lib/svg'
import { useIndex } from '../../context/context'
import { cloneElement } from 'react'

export default function PostList({ isLoading, pageList, hasMore, nextPage, children }) {
	const { overflowRef } = useIndex()

	return (
		<div className='flex flex-col w-full md:w-10/12 lg:w-8/12 xl:w-11/12 mx-auto'>
			{isLoading
				? <LoadingSpinner className='mx-auto my-4 w-12 h-12 animate-spin fill-blue-400 dark:fill-slate-100' viewBox='0 0 19 19' />
				: pageList.length
					? <InfiniteScroll
						dataLength={pageList.length}
						next={() => nextPage()}
						hasMore={hasMore()}
						loader={<LoadingSpinner className='mx-auto my-2 w-10 h-10 animate-spin fill-blue-400 dark:fill-slate-100' viewBox='0 0 20 20' />}
						scrollableTarget={overflowRef}
						endMessage={<p className='my-4 text-xl text-center'>No hay más publicaciones</p>}
					>
						{pageList.map(
							(item, i) => (
								<Link key={i} to={`${item.path}`}>
									{ cloneElement(children, {key:i, markdown:item}) }
								</Link>
							)
						)}
					</InfiniteScroll>
					: <div className='flex m-auto'>No hay publicaciones</div>
			}
		</div >
	)
}

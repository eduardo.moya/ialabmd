import { useEffect, useRef } from "react"
import { useFiles } from "../../context/context"
import { useListPublications } from "../../hooks/hooks"
import { sortByDate, sortByName } from '../../lib/utils'
import PostList from '../posts/PostList'
import PostCard from "../postcard/PostCard"

const PER_PAGE = 5

export default function Publications() {
	const {
		pageList, setPageList,
		fileList, setFileList,
		isLoading, setLoading
	} = useFiles()
	const infiniteScroll = useRef()

	useListPublications({ setFileList, setLoading })

	useEffect(() => {
		setPageList(fileList.slice(0, PER_PAGE))
	}, [fileList, setPageList])

	const nextPage = () => {
		setPageList(current => current.concat(
			fileList.slice(pageList.length, pageList.length + PER_PAGE)
		))
	}

	const hasMore = () => pageList.length < fileList.length

	return (
		<div ref={infiniteScroll} id='infiniteScroll' className='flex flex-col flex-1 mx-auto'>
			<h1 className='text-center text-3xl mx-auto mt-10 mb-8 font-semibold leading-5'>
				Listado de Publicaciones
			</h1>

			<div className='flex gap-3 justify-center md:w-10/12 lg:w-7/12 mx-auto'>
				<div className='inline-block'>
					Ordenar
				</div>

				<button className='bg-slate-500 p-1 rounded-md'
					onClick={() => setFileList(current => sortByName(current))}
				>
					Por nombre
				</button>

				<button className='bg-slate-500 p-1 rounded-md'
					onClick={() => setFileList(current => sortByDate(current))}
				>
					Por fecha
				</button>
			</div>

			<PostList isLoading={isLoading} pageList={pageList} hasMore={hasMore} nextPage={nextPage}>
				<PostCard />
			</PostList>
		</div>
	)
}

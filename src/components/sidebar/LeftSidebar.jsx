import { useEffect, useState } from "react"
import { Link } from 'react-router-dom'
import { trimFileName } from "../../lib/text"

function LeftSidebar() {
	const [files, setFiles] = useState([])
	const path = encodeURIComponent('publications/')

	useEffect(() => {
		fetch(`https://gitlab.com/api/v4/projects/40069687/repository/tree?recursive=true&per_page=100&path=${path}&ref=publications`)
			.then(res => res.json())
			.then(res => {
				const filtered = res.filter(el => el.name?.match(/^[_a-zA-Z0-9\s-]+([.,-_]*[_a-zA-Z0-9\s-]*)*\.md$/))
				setFiles(filtered)
			})
	}, [path])

	return (
		<div className='hidden lg:block md:w-1/5 bg-white dark:bg-[#0d1117]'>
			<div className='m-3 text-lg'>
				<h3 className='text-xl'>
					Últimas publicaciones
				</h3>
				{
					files.map((item, key) => (
						<Link key={key} to={`../../${item.path}`}>
							<div key={key} className='bg-slate-200 m-3 p-2 rounded  dark:bg-[#253041]'>
								{trimFileName(item.name)}
							</div>
						</Link>
					))
				}
			</div>
		</div>
	)
}

export default LeftSidebar

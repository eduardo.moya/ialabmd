import Index from "./Index"

function RightSidebar({ headings = [] }) {

	return (
		<aside className='sticky top-0 hidden md:block w-1/5 gap-4 p-3 bg-white dark:bg-transparent dark:text-white'>
			<Index headings={headings}/>
		</aside>
	)
}

export default RightSidebar

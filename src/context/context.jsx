import { createContext, useContext, useRef, useState } from "react"

const IndexContext = createContext()

export function useIndex() { return useContext(IndexContext) }

export function IndexProvider({ children }) {
	const [headings, setHeadings] = useState([])
	const overflowRef = useRef()

	return (
		<IndexContext.Provider value={{ headings, setHeadings, overflowRef }}>
			{children}
		</IndexContext.Provider>
	)
}

const FilesContext = createContext()

export function useFiles() { return useContext(FilesContext) }

export function FilesProvider({ children }) {
	const [pageList, setPageList] = useState([])
	const [fileList, setFileList] = useState([])
	const [isLoading, setLoading] = useState(true)

	return (
		<FilesContext.Provider
			value={{
				pageList, setPageList,
				fileList, setFileList,
				isLoading, setLoading
			}}
		>
			{children}
		</FilesContext.Provider>
	)
}

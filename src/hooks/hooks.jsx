import { useEffect, useRef, useState } from "react"
import { useIndex } from "../context/context"

const BASE_URL = 'https://gitlab.com/api/v4/projects'
const REPO_ID = '40069687'
const REF = 'pages'
const FETCH_TREE = 'repository/tree'
const FETCH_FILES = 'repository/files'
const PATH_POSTS = 'publicaciones'

const filter = files => files.filter(el => el.name?.match(/^[_a-zA-Z0-9\s-]+([.,-_]*[_a-zA-Z0-9\s-]*)*\.md$/))
const log = (arr = []) => arr.length > 0 ? arr[0] : {}

export function useGetMarkdown({ fileName, setMarkdown, setLoading, path }) {
	const ref = REF ? `ref=${REF}` : ''

	useEffect(() => {
		const url = `${BASE_URL}/${REPO_ID}/${FETCH_FILES}/${encodeURIComponent(path + '/' + fileName)}/raw?${ref ? ref : ''}`
		
		fetch(url)
			.then(res => res.text())
			.then(res => {
				setMarkdown(res)
				setLoading(false)
			})
	}, [fileName, ref, setMarkdown, setLoading, path])
}

export function useListPublications({ setFileList, setLoading, path = PATH_POSTS }) {
	useEffect(() => {
		let fileTree = []
		let fileLogs = []
		const url = `${BASE_URL}/${REPO_ID}/${FETCH_TREE}?id=${REPO_ID}&ref=${REF}&path=${path}&recursive=false`
		
		fetch(url)
		.then(res => {
			fileTree = res.json()
			return fileTree
		})
		.then(fileTree =>
			Promise.all(fileTree.map(item => fetch(`${BASE_URL}/${REPO_ID}/${FETCH_FILES}/${encodeURIComponent(item.path)}/blame?ref=${REF}`)))
			.then(res =>
				Promise.all(res.map(item => item.json()))
				.then(res => {
					fileLogs = res
					let list = filter(
						fileTree.map((item, i) => ({
							name: item.name,
							path: item.path,
							date: log(fileLogs[i])?.commit?.authored_date || 'no date',
							author: log(fileLogs[i])?.commit?.author_name || 'no author',
							lines: log(fileLogs[i])?.lines?.slice(0, 20) || []
						}))
					)
					setFileList(list)
					setLoading(false)
				})
				.catch(error => console.warn(error))
			)
			.catch(error => console.warn(error))
		)
		.catch(error => console.warn(error))
	}, [setFileList, setLoading, path])
}

export function useIndexObserver(setActiveId) {
	const { headings } = useIndex()
	const headingElementsRef = useRef({})
	
	useEffect(() => {
		const headingElements = Array.from( document.querySelectorAll('h2, h3, h4') )

		const callback = (headings) => {
			headingElementsRef.current = headings.reduce( (map, headingElement) => {
				map[headingElement.target.id] = headingElement
				return map
			}, headingElementsRef.current)
	
			// Get all headings that are currently visible on the page
			const visibleHeadings = []
			// console.log('headingElement', headingElementsRef.current)
			Object.keys(headingElementsRef.current).forEach( key => {
				const headingElement = headingElementsRef.current[key]
				if (headingElement.isIntersecting) visibleHeadings.push(headingElement)
			})
	
			const getIndexFromId = (id) => headingElements.findIndex( heading => heading.id === id )

			// If there is only one visible heading, this is our "active" heading
			if(visibleHeadings.length === 1) setActiveId(visibleHeadings[0].target.id)
			// If there is more than one visible heading,
        	// choose the one that is closest to the top of the page
			else if(visibleHeadings.length > 1) {
				const sortedVisibleHeadings = visibleHeadings.sort(
					(a,b) => getIndexFromId(a.target.id) > getIndexFromId(b.target.id)
				)
				setActiveId(sortedVisibleHeadings[0].target.id)
			}
		}

		const observer = new IntersectionObserver(callback, {
			rootMargin: '0px 0px -40% 0px',
		})

		headingElements.shift() // remove the main HTML Header from the array
		headingElements.forEach( item => observer.observe(item) )

		return () => observer.disconnect()
	}, [headings, setActiveId])
}

export function useResize(refObject, breakpoints) {
	const firstQuery = Object.keys(breakpoints[0])[0]
	const [breakSize, setBreakSize] = useState(firstQuery)

	const observer = useRef(
		new ResizeObserver( entries => {
			const width = entries[0].contentRect.width
			setBreakSize(findBreakPoint(breakpoints, width))
		})
	)

	useEffect(() => {
		const watcher = observer.current
		const target = refObject.current
		if(refObject.current) watcher.observe(target)
		/*
			The ref value 'observer.current' will likely have changed by the time this 
			effect cleanup function runs. If this ref points to a node rendered by React, 
			copy 'observer.current' to a variable inside the effect, and use that 
			variable in the cleanup function.eslintreact-hooks/exhaustive-deps 
		*/ // Warning ^^^^ ----> vvvvvvv
		// return () => observer.current.unobserve()

		return () => watcher.unobserve(target)
		
	}, [refObject, observer])

	return breakSize
}

function findBreakPoint(breakpoints, width) {
	const breakpointIndex = breakpoints
		.map(x => Object.values(x)[0])
		.findIndex(x => width < x);

	if (breakpointIndex === -1) {
		return Object.keys(breakpoints[breakpoints.length - 1])[0];
	}

	return Object.keys(breakpoints[breakpointIndex])[0];
}

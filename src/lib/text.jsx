
export const trimFileExtension = (input='') => input.substring(0, input.lastIndexOf('.')) || input

export const trimFileName = (input='') => input.length > 23 ? `${input.substring(0, 20)}...` : trimFileExtension(input)

export const trimFileNameN = (input='',length='') => input.length > length ? `${input.substring(0, length)}...` : trimFileExtension(input)

export const parseEvent = markdown => {
	if(typeof markdown !== 'string') return

	const regex = /## (\w*)\n+([\w\s,:.\n]*)*/g
	const matches = markdown.match(regex).map(item => item.slice(3))
	const result = Object.fromEntries(
		matches.map(str => str.split('\n'))
			.map(arr => arr.filter(item => item))
			.map(arr => arr.length > 2 ? [arr[0], arr.slice(1)] : arr)
	)

	return result
}

const years = date => (Date.now() - (new Date(date))) / 365 / 24 / 60 / 60 / 1000
const months = date => (Date.now() - (new Date(date))) / 1000 / 60 / 60 / 24 / 30
const weeks = date => (Date.now() - (new Date(date))) / 1000 / 60 / 60 / 24 / 7
const days = date => (Date.now() - (new Date(date))) / 1000 / 60 / 60 / 24
const hours = date => (Date.now() - (new Date(date))) / 1000 / 60 / 60
const minutes = date => (Date.now() - (new Date(date))) / 1000 / 60

export const getTimeLapse = (date) => {
	if (!date) return ''
	else if (years(date) >= 1) return `${Math.floor(years(date))} ${years(date) > 2 ? 'day' : 'days'} ago`
	else if (months(date) >= 1) return `${Math.floor(months(date))} ${months(date) < 2 ? 'month' : 'months'} ago`
	else if (weeks(date) >= 1) return `${Math.floor(weeks(date))} ${weeks(date) < 2 ? 'week' : 'weeks'} ago`
	else if (days(date) >= 1) return `${Math.floor(days(date))} ${days(date) < 2 ? 'day' : 'days'} ago`
	else if (hours(date) >= 1) return `${Math.floor(hours(date))} ${hours(date) < 2 ? 'hour' : 'hours'} ago`
	else if (minutes(date) >= 1) return `${Math.floor(minutes(date))} ${minutes(date) < 2 ? 'minute' : 'minutes'} ago`
}

export const fechaEN = item => {
	if(!item) return

	const monthEN = {
		Ene: 'Jan', Abr: 'Apr', Ago: 'Aug', Dic: 'Dec',
		ene: 'Jan', abr: 'Apr', ago: 'Aug', dic: 'Dec',
		Enero: 'Jan', Abril: 'Apr', Agosto: 'Aug', Diciembre: 'Dec',
		enero: 'Jan', abril: 'Apr', agosto: 'Aug', diciembre: 'Dec',
	}

	// const regex = /[0-9]{1,2}[\s-.,\/](Ene|Abr|Ago|Dic)[0-9]{2,4}/gi
	const regex = /(Ene|Abr|Ago|Dic|Enero|Abril|Agosto|Diciembre)/gi

	return item.replace(regex, (matched) => monthEN[matched])
}

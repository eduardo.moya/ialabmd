
export function getNestedHeadings(headingElements) {
	if (!headingElements) return
	const nestedHeadings = [];

	headingElements.forEach((heading, index) => {
		const { innerText: title, id } = heading;

		if (heading.nodeName === "H2")
			nestedHeadings.push({ id, title, items: [] });
		else if (heading.nodeName === "H3" && nestedHeadings.length > 0)
			nestedHeadings[nestedHeadings.length - 1].items.push({ id, title })
	});

	return nestedHeadings;
}

export function sortByName(list) {
	const current = new Array(...list)

	return current.sort( (A,B) => {
		let a = A.name?.toLowerCase()
		let b = B.name?.toLowerCase()
		if(a < b) return -1
		if(a > b) return 1
		return 0
	})
}

export function sortByDate(list) {
	const current = new Array(...list)

	return current.sort( (A,B) => {
		let a = A.date?.toLowerCase()
		let b = B.date?.toLowerCase()
		if(a < b) return 1
		if(a > b) return -1
		return 0
	})
}
